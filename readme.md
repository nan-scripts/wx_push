# 干饭&&下班企业微信告警

## 该脚本主要是自动推送表情包
## 目前有两种类型，img\0存放干饭表情包 \1存放下班表白，结合centos系统定时任务，可实现定时推送
## 脚本wx_url处配置机器人key
```python
wx_url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={key}'
```

## centos7定时任务配置
### 安装crotab
```bash
yum -y install cronie
```
### 配置定时任务
```bash
crontab -b 
# 在文件末尾添加
59 11 * * * /usr/bin/python3 /root/script/wx_push/wx_push.py 0
30 18 * * * /usr/bin/python3 /root/script/wx_push/wx_push.py 1
```

