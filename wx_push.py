# -*- coding: utf-8 -*-
import datetime
import base64
import hashlib
import sys
import random
import requests
import os
import platform
# from requests.packages.urllib3.exceptions import InsecureRequestWarning
# requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# rq = HTMLSession()
wx_url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key='


img_path = os.path.dirname(__file__)


#随机获取图片
def get_img(num):
    # 判断操作系统
    if platform.system() == 'Windows':
        img_path = os.path.dirname(__file__)
        file_path = img_path + '\\img\\' + num + '\\'
    elif platform.system() == 'Linux':
        img_path = os.getcwd()
        file_path = img_path + '/img/' + num + '/'
    # file_list = os.listdir(file_path)
    img_name = random.choice(os.listdir(file_path))
    
    return file_path + img_name
# 计算图片md5和base64
def calc_img(img_path):
    img_data = {} 
    # 把图片转换成base64
    with open(img_path, 'rb') as f:
        encodestr = base64.b64encode(f.read())
        img_base64 = str(encodestr,'utf-8')
        f.close()
    # 图片MD5
    with open(img_path, 'rb') as f:
        img_md5 = hashlib.md5(f.read()).hexdigest()
        f.close()
    img_data['base64'] = img_base64
    img_data['md5'] = img_md5

    return img_data

def wx_push(data):
    base64_data = data['base64']
    md5_data = data['md5']
    print(md5_data)
    headers = {'Content-type':'application/json'}
    conntent = {"msgtype": "image",
            "image": {"base64": base64_data,
                      "md5": md5_data
                     }
            }
    r = requests.post(wx_url,json=conntent)
    print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"),"发送状态:",r.text)
    return 1

if __name__ == "__main__":
    num = sys.argv[1]
    if num == "0":
        img_path = get_img(num)
        data = calc_img(img_path)
        # print(data['md5'])
        wx_push(data)
    if num == "1":
        img_path = get_img(num)
        data = calc_img(img_path)
        # print(data['md5'])
        wx_push(data)
    


        
